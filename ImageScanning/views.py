from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from django.http import HttpResponse
from .serializers import FileSerializer
import json
import cv2
import os
#import base64
import numpy as np

from .utils.commons.validator import Validation
from .utils.commons.imageprocessing import Image_Handler
from .utils.commons.filters import Filters

# LOADS
#data_logging = True
fil = Filters()
handler = Image_Handler()
validator = Validation()
Options = ["Threshold", "Adaptive Threshold", "HDR Color Boost", "Default Enhance Text", "Default Enhance Object",
           'Apply Sharpness', 'Adjust Exposure']


class ImageServiceView(APIView):
    def post(self, request):
        output_dict = dict()
        file_serializer = FileSerializer(data=request.data)
        if file_serializer.is_valid():
            file_serializer.save()
            option = request.POST['option']
            if 'file' in request.FILES:  # get file from request
                if validator.validate_format(request.FILES['file'].name):
                    file_path = file_serializer.data.get('file')
                    # name, _ = os.path.splitext(file_path)
                    filename = cv2.imread(file_path)
                    h, t = os.path.split(file_path)
                    img = handler.preprocessing_options(option=option, fil=fil, image_np=filename)
                    print(file_path)
                    preprocessed_files = 'media/preprocessed/preprocessed'+t
                    print(preprocessed_files)
                    #cv2.imshow("11",img)
                    #cv2.waitKey(0)
                    #cv2.destroyAllWindows()
                    cv2.imwrite(preprocessed_files,img)
                    output_dict['filepath'] = preprocessed_files
                    print('Uploaded file is :', request.FILES['file'].name,output_dict)
                    resp =  HttpResponse(json.dumps(output_dict), status=status.HTTP_201_CREATED)
                    resp["Access-Control-Allow-Origin"] = "*"
                    return resp

        else:
            resp = HttpResponse(json.dumps(file_serializer.errors), status=status.HTTP_400_BAD_REQUEST)
            resp["Access-Control-Allow-Origin"] = "*"
            return resp

class Option(APIView):
    """docstring for options"""
    def get(self, request):
        resp = HttpResponse(json.dumps({"options":Options}))
        resp["Access-Control-Allow-Origin"] = "*"
        return resp