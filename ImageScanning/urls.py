from django.conf.urls import url
from .views import ImageServiceView  # SignatureDetectionView, LogoDetectionView, LogoSimilarityView,OcrServiceView
from .views import Option
urlpatterns = [
    url('scanner/', ImageServiceView.as_view(),name="file-upload"),
    url('options/', Option.as_view()),
]
