from django.db import models

# Create your models here.

from django.db import models
#from .apps import EnvisageConfig
import os
#from camscanner.settings import MEDIA_URL

class File(models.Model):
    # file = models.FileField(blank=False, null=False, upload_to=os.path.join(MEDIA_URL, 'envisage', 'signature', 'input'))
    file = models.FileField(blank=False, null=False, upload_to='input')

    def __str__(self):
        return self.file.name

'''
class MultipleFiles(models.Model):
    file1 = models.FileField(blank=False, null=False, upload_to=os.path.join('media', 'envisage', 'signature', 'upload'))
    file2 = models.FileField(blank=False, null=False, upload_to=os.path.join('media', 'envisage', 'signature', 'upload'))

    def __str__(self):
        return self.file1
'''