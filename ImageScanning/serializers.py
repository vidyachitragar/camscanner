from rest_framework import serializers
from .models import File
#from .models import MultipleFiles

class FileSerializer(serializers.ModelSerializer):
    class Meta:
        model = File
        fields = "__all__"

'''
class MultiFileSerializer(serializers.ModelSerializer):
    class Meta:
        model = MultipleFiles
        fields = "__all__"
'''