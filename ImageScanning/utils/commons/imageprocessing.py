import asyncio
import cv2
import numpy as np
import json
from PIL import Image
import io

import base64

Options = ["Threshold","Adaptive Threshold","HDR Color Boost",
           "Default Enhance Text","Default Enhance Object",'Apply Sharpness','Adjust Exposure']
class Image_Handler(object):
    def __init__(self):
        pass

    def file_parser(self,form,content,time = 0):
        if '.tiff' in form or '.tif' in form or '.TIF' in form or '.TIFF' in form:
            open("temp.tif",'wb').write(content)
            print("Custom Debug|--|Reading Multiple files")
            ret, img = cv2.imreadmulti("temp.tif")
            return len(img), img
        else:
            print("Custom Debug|--|Reading single file")
            nparr = np.frombuffer(content, np.uint8)
            img_np = cv2.imdecode(nparr, cv2.IMREAD_COLOR)
            if len(img_np.shape)<3: 
                print("Custom debug  broken image data trying alternate conversion")
                im = Image.open(io.BytesIO(content))
                print(im)
                img_np = np.asarray(im)[:,:,::-1]

                print(img_np.shape)
                if np.all(img_np==0):
                    raise Exception
            return 1, [img_np]

    def preprocessing_options(self, option, image_np, fil, timer = 0):
        try:
            print('custom debug hit3 getting options')
            print("option : ", option)
            if option in Options and Options.index(option)== 0:
                img = fil.thresh(image_np)
                return img
                # return cv2.imencode('.png',img)[1]
                # return cv2.imencode('.png',img)[1].tostring()
                #return base64.b64encode(cv2.imencode('.png', img)[1]).decode()

            elif option in Options and Options.index(option)== 1:
                img = fil.adaptive_thresh(image_np)
                return img
                # return cv2.imencode('.png',img)[1]
                #return cv2.imencode('.png',img)[1].tostring()
                # return base64.b64encode(cv2.imencode('.png', img)[1]).decode()
                
            elif option in Options and Options.index(option)== 2:
                img = fil.Optimize_image(image_np)
                #return cv2.imencode('.png',img)[1].tostring()
                # return cv2.imencode('.png',img)[1]
                return img
                # return base64.b64encode(cv2.imencode('.png', img)[1]).decode()
                
            elif option in Options and Options.index(option)== 3:
                img = fil.enhance_image(image_np,s1 = 30,s2 = 100,r2=0.9,r1=0.05)
                #return cv2.imencode('.png',img)[1].tostring()
                # return cv2.imencode('.png',img)[1]
                return img
                # return base64.b64encode(cv2.imencode('.png', img)[1]).decode()
                
            elif option in Options and Options.index(option)== 4:
                img = fil.enhance_image(image_np)
                # return cv2.imencode('.png',img)[1]
                return img
                #return cv2.imencode('.png',img)[1].tostring()
                # return base64.b64encode(cv2.imencode('.png', img)[1]).decode()
                
            elif option in Options and Options.index(option)== 5:
                img = fil.sharpen_image(image_np)
                return img
                # return cv2.imencode('.png',img)[1]
                #return cv2.imencode('.png',img)[1].tostring()
                # return base64.b64encode(cv2.imencode('.png', img)[1]).decode()
                
            elif option in Options and Options.index(option)== 6:
                img = fil.adjust_gamma(image_np,0.5)
                return img
                # return cv2.imencode('.png',img)[1]
                #return cv2.imencode('.png',img)[1].tostring()
                # return base64.b64encode(cv2.imencode('.png', img)[1]).decode()

            else:
                print("custom debug [--]:no option selected")
                return image_np
                # return cv2.imencode('.png',image_np)[1]
                #return cv2.imencode('.png',image_np)[1].tostring()
                # return base64.b64encode(cv2.imencode('.png', image_np)[1]).decode()
                
        except:
            print('custom debug error in options')
            return image_np
            # return cv2.imencode('.png',image_np)[1]
            #return cv2.imencode('.png',image_np)[1].tostring()
            # return base64.b64encode(cv2.imencode('.png', image_np)[1]).decode()

