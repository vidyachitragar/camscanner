# -*- coding: utf-8 -*-
"""
Created on Thu Jun 20 15:57:08 2019

@author: Manish_S
"""

import cv2
import numpy as np
from scipy import misc, signal
import numpy as np



class Filters(object):
    def __init__(self):
        print("Image Filters Loaded")
        
    def sharpen_image(self,img):    #gets a color image
        im=img/255
        sharpen_kernel = np.array([[0, -1, 0], [-1, 5, -1], [0, -1, 0]])
        imsharpened = np.ones(im.shape)
        for i in range(3):
            imsharpened[...,i] = np.clip(signal.convolve2d(im[...,i], sharpen_kernel, mode='same', boundary="symm"),0,1)
        return np.uint8(imsharpened*255)
    
    
    def enhance_image(self,image, s1 = 60, r1 = 0.4, s2 = 80, r2 = 0.25 , apply_sharpness = False, threshold_image = False):
        try:
            img = cv2.imread(image)
        except:
            img = image
        dst = cv2.edgePreservingFilter(img, flags = 1, sigma_s = s2, sigma_r = r1)
        dst1 = cv2.detailEnhance(dst, sigma_s = s1, sigma_r = r2)
        opimage = cv2.edgePreservingFilter(dst1, flags = 1, sigma_s = s1, sigma_r = r1)
        
        if apply_sharpness == True:
            opimage = self.sharpen_image(opimage)
        if threshold_image == True:
            grey_image = cv2.cvtColor(opimage, cv2.COLOR_BGR2GRAY)
            opimage = cv2.adaptiveThreshold(grey_image, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C,
                                             cv2.THRESH_BINARY, 11, 50)
        return opimage
    
    
    
    def adaptive_thresh(self,image):
        try:
            img = cv2.imread(image,0)
        except:
            if len(image.shape)==2:
                img = image
            elif len(image.shape) == 3:
                img = cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)
                
        th3 = cv2.adaptiveThreshold(img,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY,11,2)
        return th3
    
    
    def thresh(self,image):
        img = image
        ret,thresh1 = cv2.threshold(img,127,255,cv2.THRESH_BINARY)
        return thresh1
    
    
    
    def adjust_gamma(self, image, gamma=0.3):
       invGamma = 1.0 / gamma
       table = np.array([((i / 255.0) ** invGamma) * 255
          for i in np.arange(0, 256)]).astype("uint8")
    
       return cv2.LUT(image, table)



    def Optimize_image(self,image):
        try:
            img = cv2.imread(image)
        except:
            img = image
        if len(image.shape) == 3:
            hsv = cv2.cvtColor(img,cv2.COLOR_BGR2HSV)
            hs = hsv
            hsv[:,:,2]=30
            hs[:,:,1]=150
            mo = cv2.cvtColor(hsv,cv2.COLOR_HSV2BGR)
            mi = cv2.cvtColor(hs,cv2.COLOR_HSV2BGR)
            im = cv2.add(img,mo)
            im = cv2.add(im,mo)
            im = cv2.add(im,mi)
            return im
        else:
            return image
